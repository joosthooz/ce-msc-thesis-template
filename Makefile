LATEX=latex
PDFLATEX=pdflatex
BIBTEX=bibtex

SOURCES=thesis.tex ack.tex conclusions.tex cover.tex front.tex intro.tex results.tex

.PHONY: clean tarball images-pdf

default: pdf

dvi: thesis.dvi
pdf: thesis.pdf

thesis.dvi: $(SOURCES) thesis.bib images-eps
	$(LATEX) thesis.tex
	$(BIBTEX) thesis
	$(LATEX) thesis.tex
	$(LATEX) thesis.tex

thesis.pdf: $(SOURCES) thesis.bib images-pdf
	$(PDFLATEX) thesis.tex
	$(BIBTEX) thesis
	$(PDFLATEX) thesis.tex
	$(PDFLATEX) thesis.tex

images-eps:
	$(MAKE) -C images/ eps

images-pdf:
	$(MAKE) -C images/ pdf

clean:
	$(MAKE) -C images/ clean
	rm -f $(SOURCES:.tex=.aux) thesis.bbl thesis.blg thesis.dvi thesis.lof thesis.log thesis.lot thesis.pdf thesis.toc thesis.out thesis.tar.gz

tarball:
	tar -czf thesis.tar.gz ce.cls Makefile thesis.bib $(SOURCES) images/
